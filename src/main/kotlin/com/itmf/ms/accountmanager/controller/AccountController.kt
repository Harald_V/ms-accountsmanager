package com.itmf.accountmanager.controller

import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/accounts")
class AccountController {

	@GetMapping("/status/check")
	fun getStatus(): String? = "ok"
	
}